import React, { useEffect, useState } from 'react'
import 'sweetalert2/dist/sweetalert2.css'
import Swal, { SweetAlertIcon, SweetAlertResult } from 'sweetalert2'
import logo from './logo.svg'
import './App.css'

function SweetAlert(props: {
  isOpen: boolean
  title: string
  text: string
  icon: SweetAlertIcon
  confirmButtonText: string
  onResult: (result: SweetAlertResult<any>) => void
}) {
  useEffect(() => {
    if (props.isOpen) {
      Swal.fire({
        title: props.title,
        text: props.text,
        icon: props.icon,
        confirmButtonText: props.confirmButtonText,
      }).then(props.onResult)
    }
  }, [props.isOpen])
  return <></>
}

function App() {
  const [isOpen, setIsOpen] = useState(false)
  const [result, setResult] = useState<SweetAlertResult | null>(null)
  return (
    <div className="App">
      <header className="App-header">
        <button onClick={() => setIsOpen(true)}>Open</button>
        <div>
          result:
          <pre>
            <code>{JSON.stringify(result, null, 2)}</code>
          </pre>
        </div>
        <SweetAlert
          isOpen={isOpen}
          title="Confirmation"
          text="Do you want to continue?"
          icon="question"
          confirmButtonText="Yup"
          onResult={result => {
            console.log(result)
            setResult(result)
            setIsOpen(false)
          }}
        />
      </header>
    </div>
  )
}

export default App
